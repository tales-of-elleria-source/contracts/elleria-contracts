const advanceTime = async (time, from) => {
  return new Promise((resolve, reject) => {
    web3.currentProvider.send(
      {
        jsonrpc: '2.0',
        method: 'evm_increaseTime',
        params: [time],
        id: new Date().getTime(),
        from
      },
      (error, response) => {
        if (error) {
          reject(error);
        } else {
          resolve(response);
        }
      }
    );
  }).then(() => {
    return web3.eth.personal.sendTransaction({ method: 'evm_mine', from });
  });
};

const setBlockTime = async (timestamp, from) => {
  return new Promise((resolve, reject) => {
    web3.currentProvider.send(
      {
        jsonrpc: '2.0',
        method: 'evm_mine',
        params: [timestamp],
        id: new Date().getTime(),
        from
      },
      (error, response) => {
        if (error) {
          reject(error);
        } else {
          resolve(response);
        }
      }
    );
  });
};

const wait = ms => {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

const mineBlock = from => {
  return web3.eth.personal.sendTransaction({ method: 'evm_mine', from });
}

module.exports = { advanceTime, wait, mineBlock, setBlockTime };