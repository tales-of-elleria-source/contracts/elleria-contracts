const ElmToken = artifacts.require("ElleriumTokenERC20v2");
const BulkTransfer = artifacts.require("ERC20BulkTransfer");
const BN = require('bn.js');

contract('ERC20BulkTransfer', (accounts) => {

    const contractsOwner = accounts[0];
    const userAddress = accounts[1];
    let elmContractInstance;
    let bulkTransferContractInstance;

    before(async () => {
        // Deploy elmContractInstance.
        elmContractInstance = await ElmToken.new({ from: contractsOwner });
        bulkTransferContractInstance = await BulkTransfer.new({ from: contractsOwner });

        await elmContractInstance.approve(bulkTransferContractInstance.address, new BN(1000000));
    });

    
    it("should successfully bulk transfer", async function() {
        const recipientAddresses = [accounts[1], accounts[2], accounts[3]];
        const transferAmounts = [new BN(100), new BN(200), new BN(300)];
        const result = await bulkTransferContractInstance.bulkTransfer(elmContractInstance.address, recipientAddresses, transferAmounts, { from: contractsOwner });

        for (let i = 0; i < recipientAddresses.length; i += 1) {
            const recipientBalance = await elmContractInstance.balanceOf(recipientAddresses[i]);
            assert.equal(recipientBalance.toString(10), transferAmounts[i].toString(10));
        }
    });

    it("should revert when recipients array and amounts array lengths are not equal", async function() {
        const recipientAddresses = [accounts[1], accounts[2]];
        const transferAmounts = [new BN(100), new BN(200), new BN(300)];
  
        try {
            elmContractInstance.bulkTransfer(elmContractInstance.address, recipientAddresses, transferAmounts, { from: contractsOwner }),
            assert.fail('Expected revert not received');
        } catch (error) {
            assert(error.message, 'BadUserInput');
        }
    });

    it("should revert when trying to transfer more than balance", async function() {
        const recipientAddresses = [accounts[1], accounts[2], accounts[3]];
        const transferAmounts = [new BN(100), new BN(200), new BN(1000000000000)]; // last value exceeds contractsOwner balance

        try {
            elmContractInstance.bulkTransfer(elmContractInstance.address, recipientAddresses, transferAmounts, { from: contractsOwner }),
            assert.fail('Expected revert not received');
        } catch (error) {
            assert(error.message, 'ValueOverflow');
        }
    });
});
