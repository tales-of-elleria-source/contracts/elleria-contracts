const signatureContract = artifacts.require("VerifySignature");
const { AbiCoder, ethers, keccak256, toUtf8Bytes } = require('ethers');

contract('VerifySignature', (accounts) => {
    
    const EIP712DOMAIN_TYPEHASH = keccak256(
        toUtf8Bytes("EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)")
      );
      
    let DOMAIN_SEPARATOR;
    
    // Exclusively for testing purposes to have consistent values.
    // User-generated signatures are signed by this third-party signer.
    const privateKey = 'a8f2223c9442b56d469de56e423a8f9928154625a72e4ac1f1541ec7ba1766e3'
    const signer = new ethers.Wallet(privateKey);

    let signatureContractInstance;

    before(async () => {
        // Just deploy once since the contract is stateless.
        const address = await signer.getAddress();
        signatureContractInstance = await signatureContract.new(421613, { from: address });

        DOMAIN_SEPARATOR = keccak256(
            AbiCoder.defaultAbiCoder().encode(
                ['bytes32', 'bytes32', 'bytes32', 'uint256', 'address'],
                [
                    EIP712DOMAIN_TYPEHASH,
                    keccak256(toUtf8Bytes('Tales of Elleria')),
                    keccak256(toUtf8Bytes('V4')),
                    421613,
                    signatureContractInstance.address
                ]
            )
            );
    });

    it('should return true for a correctly generated signature for verify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const amount = '100';
        const message = "test message";
        const nonce = 1;

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getMessageHash(
            DOMAIN_SEPARATOR, to, amount, message, nonce
        );
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        // Test the verification.
        const result = await signatureContractInstance.verify(
            from, to, amount, message, nonce, signature
        );
        assert.equal(result, true);
    });

    it('should return false if from specified wrongly for verify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const amount = '100';
        const message = "test message";
        const nonce = 1;

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getMessageHash(DOMAIN_SEPARATOR, to, amount, message, nonce);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        // Test invalid verification.
        const result = await signatureContractInstance.verify(accounts[1], to, amount, message, nonce, signature);
        assert.equal(result, false);
    });

    it('should return false if to specified wrongly for verify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const amount = '100';
        const message = "test message";
        const nonce = 1;

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getMessageHash(DOMAIN_SEPARATOR, to, amount, message, nonce);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);

        // Test invalid verification.
        const result = await signatureContractInstance.verify(from, accounts[0], amount, message, nonce, signature);
        assert.equal(result, false);
    });

    it('should return false if amount specified wrongly for verify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const amount = '100';
        const message = "test message";
        const nonce = 1;

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getMessageHash(DOMAIN_SEPARATOR, to, amount, message, nonce);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        // Test invalid verification.
        const result = await signatureContractInstance.verify(from, to, 1000, message, nonce, signature);
        assert.equal(result, false);
    });

    it('should return false if message specified wrongly for verify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const amount = '100';
        const message = "test message";
        const nonce = 1;

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getMessageHash(DOMAIN_SEPARATOR, to, amount, message, nonce);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
    
        // Test invalid verification.
        const result = await signatureContractInstance.verify(from, to, amount, 'wrong', nonce, signature);
        assert.equal(result, false);
    });

    it('should return false if nonce specified wrongly for verify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const amount = '100';
        const message = "test message";
        const nonce = 1;

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getMessageHash(DOMAIN_SEPARATOR, to, amount, message, nonce);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        // Test invalid verification.
        const result = await signatureContractInstance.verify(from, to, amount, message, 2, signature);
        assert.equal(result, false);
    });

    it('should return false if signature specified wrongly for verify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const amount = '100';
        const message = "test message";
        const nonce = 1;

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getMessageHash(DOMAIN_SEPARATOR, to, amount, message, nonce);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        // Test invalid verification.
        const result = await signatureContractInstance.verify(from, to, amount, message, nonce, 
            '0x007390c49f4bb836730e36366229d02d321e6a904914d6526579030fab0387ea5713dfb7a6483e8feef20f18a95c6a74af43cf51f2898ce9601e442d7c9c0cff1b'
        );
        assert.equal(result, false);
    });

    it('should throw error if invalid signature length for verify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const amount = '100';
        const message = "test message";
        const nonce = 1;

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getMessageHash(DOMAIN_SEPARATOR, to, amount, message, nonce);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        try {
            // Test invalid verification.
            await signatureContractInstance.verify(from, to, amount, message, nonce, '0x001122334455');
            assert.fail("Expected revert not received");
        } catch (error) {
            assert.isTrue(error.message.includes("invalid signature length"), error.toString());
        }
    });

    it('should return true for a correctly generated signature for bigVerify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const data = [1, 2, 3, 4, 5];

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getBigMessageHash(DOMAIN_SEPARATOR, to, data);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        // Test the verification.
        const result = await signatureContractInstance.bigVerify(from, to, data, signature);
        assert.equal(result, true);
    });
    
    it('should return false if from specified wrongly for bigVerify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const data = [1, 2, 3, 4, 5];

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getBigMessageHash(DOMAIN_SEPARATOR, to, data);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        // Test invalid verification.
        const result = await signatureContractInstance.bigVerify(accounts[1], to, data, signature);
        assert.equal(result, false);
    });

    it('should return false if to specified wrongly for bigVerify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const data = [1, 2, 3, 4, 5];

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getBigMessageHash(DOMAIN_SEPARATOR, to, data);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        // Test invalid verification.
        const result = await signatureContractInstance.bigVerify(from, accounts[0], data, signature);
        assert.equal(result, false);
    });

    it('should return false if data specified wrongly for bigVerify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const data = [1, 2, 3, 4, 5];

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getBigMessageHash(DOMAIN_SEPARATOR, to, data);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        // Test invalid verification.
        const result = await signatureContractInstance.bigVerify(from, to, [5, 4, 3, 2, 1], signature);
        assert.equal(result, false);
    });

    it('should return false if data specified wrongly for bigVerify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const data = [1, 2, 3, 4, 5];

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getBigMessageHash(DOMAIN_SEPARATOR, to, data);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        try {
        // Test invalid verification.
        const result = await signatureContractInstance.bigVerify(from, to, '1 2 3 4 5', signature);
        assert.fail("Expected revert not received");
        } catch (error) {
            assert.isTrue(typeof error.message !== 'undefined');
        }
    });

    it('should return false if signature specified wrongly for bigVerify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const data = [1, 2, 3, 4, 5];

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getBigMessageHash(DOMAIN_SEPARATOR, to, data);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        // Test invalid verification.
        const result = await signatureContractInstance.bigVerify(from, to, data, 
            '0x007390c49f4bb836730e36366229d02d321e6a904914d6526579030fab0387ea5713dfb7a6483e8feef20f18a95c6a74af43cf51f2898ce9601e442d7c9c0cff1b'
        );
        assert.equal(result, false);
    });

    it('should throw an error if invalid signature length for bigVerify', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const data = [1, 2, 3, 4, 5];

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getBigMessageHash(DOMAIN_SEPARATOR, to, data);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);

        try {
            await signatureContractInstance.bigVerify(from, to, data, '0x1122334455');
            assert.fail("Expected revert not received");
        } catch (error) {
            assert.isTrue(error.message.includes("invalid signature length"), error.toString());
        }
    });

    it('should reject signatures with malleable s', async () => {
        const from = accounts[0];
        const to = accounts[1];
        const amount = '100';
        const message = "test message";
        const nonce = 1;

        // Mimic backend signature generation.
        const messageHash = await signatureContractInstance.getMessageHash(DOMAIN_SEPARATOR, to, amount, message, nonce);
        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);
        
        // Create a malleable s value and construct the malleable signature
        try {
        const { r, s, recoveryParam } = ethers.Signature.from(signature);
        const SECP256K1_N = BigInt('0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141');
        const sNew = SECP256K1_N - BigInt(ethers.toQuantity(s));
        const malleableSignature = ethers.Signature.from({ r, s: sNew, recoveryParam }).serialized;
        
        } catch (error) {
            // TODO :: Find a way to generate a malleable signature, since ethers automatically throws an error.
            // This check should be suffi
            // assert.isTrue(error.message.includes("invalid signature 's' value"), error.toString());
            assert.isTrue(error.message.includes("non-canonical s"), error.toString());
        }
    });
});
