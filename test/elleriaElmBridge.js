const ElleriaElmBridge = artifacts.require("ElleriaElmBridge");
const SignatureContract = artifacts.require("VerifySignature");
const ElmToken = artifacts.require("ElleriumTokenERC20v2");
const { AbiCoder, ethers, keccak256, toUtf8Bytes } = require('ethers');

const BN = require('bn.js');

contract('ElleriaElmBridge', (accounts) => {

    const EIP712DOMAIN_TYPEHASH = keccak256(
        toUtf8Bytes("EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)")
      );
      
    let DOMAIN_SEPARATOR;

    // Exclusively for testing purposes to have consistent values.
    // User-generated signatures are signed by this third-party signer (should)
    const privateKey = 'a8f2223c9442b56d469de56e423a8f9928154625a72e4ac1f1541ec7ba1766e3'
    const signer = new ethers.Wallet(privateKey);

    const contractsOwner = accounts[0];
    const userAddress = accounts[1];
    let bridgeContractInstance;
    let elmContractInstance;
    let verifierContractInstance;

    before(async () => {
        // Deploy necessary dependencies.
        // Deploy placeholder $ELM and give the bridge contract mint capabilities.
        elmContractInstance = await ElmToken.new();
 
        // Deply verifier contract
        verifierContractInstance = await SignatureContract.new(421613);

        // Deploy bridge contract
        bridgeContractInstance = await ElleriaElmBridge.new(verifierContractInstance.address, contractsOwner, elmContractInstance.address, 10, 10);
        await elmContractInstance.setApprovedAddress(bridgeContractInstance.address, true, {from: contractsOwner});

        DOMAIN_SEPARATOR = keccak256(
            AbiCoder.defaultAbiCoder().encode(
                ['bytes32', 'bytes32', 'bytes32', 'uint256', 'address'],
                [
                    EIP712DOMAIN_TYPEHASH,
                    keccak256(toUtf8Bytes('Tales of Elleria')),
                    keccak256(toUtf8Bytes('V4')),
                    421613,
                    verifierContractInstance.address
                ]
            )
            );
    });

    it('should correctly bridge ELM tokens into the game', async () => {
        const amountToBridge = new BN(BigInt(1000 * 1e18));
        
        // Mint ELM tokens to the user account.
        await elmContractInstance.mint(userAddress, amountToBridge.toString(), {from: contractsOwner});

        // Approve the bridge contract to transfer ELM on behalf of userAddress.
        await elmContractInstance.approve(bridgeContractInstance.address, amountToBridge, {from: userAddress});

        // Bridge tokens.
        await bridgeContractInstance.bridgeElleriumIntoGame(amountToBridge, {from: userAddress});

        const userBalance = await elmContractInstance.balanceOf(userAddress);
        const depositCount = await bridgeContractInstance.depositsCount();

        assert.equal(userBalance, 0, "User balance should be 0 after bridging");
        assert.equal(depositCount, 11, "Deposits count should be incremented");
    });

    it('should fail to bridge ELM tokens into the game due to insufficient balance', async () => {
        const amountToBridge = new BN(BigInt(1000 * 1e18));

        // No ELM tokens minted to the user account.
        try {
            await bridgeContractInstance.bridgeElleriumIntoGame(amountToBridge.toString(), {from: userAddress});
            assert.fail('Expected revert not received');
        } catch (error) {
            assert.ok(error.toString().includes('revert'), "The error message should contain 'revert'");
        }
    });

    it('should correctly retrieve ELM tokens from the game', async () => {
        const amountToRetrieve = new BN(BigInt(1000 * 1e18));
        const txnCount = 10;
        
        const messageHash = await verifierContractInstance.getMessageHash(
            DOMAIN_SEPARATOR,
            userAddress,
            amountToRetrieve.toString(),
            "retrieveElleriumFromGamev2",
            10
          );

        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);

        await bridgeContractInstance.retrieveElleriumFromGame(
            signature,
            amountToRetrieve.toString(),
            txnCount,
            {from: userAddress}
        );

        const userBalance = await elmContractInstance.balanceOf(userAddress);
        const withdrawCount = await bridgeContractInstance.withdrawCount();

        assert.equal(userBalance.toString(), amountToRetrieve.toString(), "User balance should be equal to the retrieved amount");
        assert.equal(withdrawCount.toString(), '11', "Withdraw count should be incremented");
    });

    it('should fail to retrieve ELM tokens due to duplicate txnCount', async () => {
        const amountToRetrieve = new BN(BigInt(1000 * 1e18));
        const txnCount = 10;
        
        const messageHash = await verifierContractInstance.getMessageHash(
            DOMAIN_SEPARATOR,
            userAddress,
            amountToRetrieve.toString(),
            "retrieveElleriumFromGamev2",
            10
          );

        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);

        try {
            await bridgeContractInstance.retrieveElleriumFromGame(
                signature, amountToRetrieve.toString(), txnCount, {from: userAddress}
            );
            assert.fail('Expected revert not received');
        } catch (error) {
            assert.ok(error.toString().includes('revert'), "The error message should contain 'revert'");
        }
    });

    it('should fail to retrieve ELM tokens due to low txnCount', async () => {
        const amountToRetrieve = new BN(BigInt(1000 * 1e18));
        const txnCount = 0;
        
        const messageHash = await verifierContractInstance.getMessageHash(
            DOMAIN_SEPARATOR,
            userAddress,
            amountToRetrieve.toString(),
            "retrieveElleriumFromGamev2",
            0
          );

        const bytesMessageHash = ethers.getBytes(messageHash);
        const signature = await signer.signMessage(bytesMessageHash);

        try {
            await bridgeContractInstance.retrieveElleriumFromGame(
                signature, amountToRetrieve.toString(), txnCount, {from: userAddress}
            );
            assert.fail('Expected revert not received');
        } catch (error) {
            assert.ok(error.toString().includes('revert'), "The error message should contain 'revert'");
        }
    });

    it('should fail to retrieve ELM tokens due to invalid signature', async () => {
        const amountToRetrieve = new BN(BigInt(1000 * 1e18));
        const txnCount = 1;

        // Again, use a function to generate a correct signature
        const invalidSignature = '0x' + '1'.repeat(130);

        try {
            await bridgeContractInstance.retrieveElleriumFromGame(
                invalidSignature, amountToRetrieve.toString(), txnCount, {from: userAddress}
            );
            assert.fail('Expected revert not received');
        } catch (error) {
            assert.ok(error.toString().includes('revert'), "The error message should contain 'revert'");
        }
    });
});
