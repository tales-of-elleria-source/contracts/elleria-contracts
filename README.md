# Tales of Elleria Smart Contract Repository
This is the store for Tales of Elleria's Smart Contracts.

Ellerian Prince (Wayne) is the maintainer of this repo.
Please send a DM on Discord to `Ellerian Prince#4509` to report any vulnerabilities. (if any)
File an issue if you require clearer documentation on specific items.

<br>Thank you!

## Tests

```
npm install
truffle develop
test
```

## Contract Addresses (Arbitrum Goerli/Arbitrum One)

| Contract         | Testnet     | Mainnet |
|--------------|-----------|------------|
| (ERC20) Ellerium  | 0x55E374162B2377E2d7773592c5D10d62535C5263 | 0xEEac5E75216571773C0064b3B591A86253791DB6  |
| (ERC20) MLP (ELM-MAGIC)      | N/A  | 0x3e8FB78Ec6Fb60575967bB07AC64e5FA9F498a4a       |
| (ERC721) Heroes      | 0xcb53B5B906F8C0a3d683dB793F565629623FF8AB  | 0x7480224eC2B98f28cEe3740c80940A2F489BF352       |
| (ERC721) Equipment      | 0x149281B5BC2c15b382524F85209f39E862E53330  | 0x236431A7F87ED9CC942427ed21dbf3F693181916       |
| (ERC1155) Relics      | 0xA8dE5c91C8B772a594d18AFDA2fc96Dd49379974  | 0x381227255eF6c5d85966b78d13e4B4a4c8719b5E       |
| MLP Staking Pool      | 0x6b87762ab1B9b21fb612a1668b2aC8de79D6E70e  | 0xf942bcf0f00aF9Ca962cd551e9Dc2C6dE3aE005c       |
| Barkeeper's Shop      | 0xd308C4385A52726Fb57A20581f0dAD05928892EF  | 0xb862f80C9D3CC28DCe6345b101460b41f3246843       |
| Relic Bridge      | 0x9B488B802305B717656242EB4beaBdD893c5992b  | 0x42cB9c24EeFDb40185E7e8274aE671ff411CFbBF       |
| Ellerium Bridge      | 0x2AC4A8f6F1a8daf1455bDEBc06c0FA4D8bdB14e5  | 0x98bf4d028Fb2172Aa06bc4C3c1da575f83507FF9       |
| Hero Bridge      | 0x99d26a041B2E39A5453A38d5A54144E713741Cf9  | 0x6CfD2EFBC6859beA905DB29e2d56836Cd3ed7006       |
| Token Vesting      | N/A  | 0x435b4cA6a7c81f2C757c14367dCaDb743e2C8406       |
| Treasury Multisig      | N/A  | 0x69832Af74774baE99D999e7F74FE3F7d5833bF84       |

## Retrieving Metadata from ERC721s
### Heroes
```
Returns metadata compliant to OpenSea's Metadata Standards.

https://wall.talesofelleria.com/api/hero/{tokenId}
```

## GraphQL for offchain transparency

The team is committed to the development of a fair, legimitate, and proper product. 

With pure on-chain games having the blockchain to act as a ledger for all transactions, we are working on making all off-chain in-game transactions accessible as well. 
This will allow for auditing for balances and events/leaderboards as required, and will also enable community apps since our APIs are exposed.

https://docs.talesofelleria.com/references/graphql-playground

## Old Error Codes

1 = Token does not exist.<br>
2 = Withdrawal failed.<br>
3 = Level can only be updated from the game address.<br>
4 = Incorrect \$MEDALS amount used for upgrade.<br>
5 = Incorrect \$ELM amount used for upgrade.<br>
6 = Insufficient experience to upgrade.<br>
7 = Invalid address attempting to change EXP.<br>
8 = Maximum Supply Reached.<br>
9 = No minting through custom contracts.<br>
10 = Recipient must have asme address as minter.<br>
11 = Presales minting is not enabled.<br>
12 = Arrays used are of invalid length. <br>
13 = You are not Whitelisted.<br>
14 = Exceeded Daily Mint Limit.<br>
15 = Minting being called with invalid address.<br>
16 = Minting using tokens is not opened.<br>
17 = Invalid address used to set EXP.<br>
18 = Invalid address used to reset EXP.<br>
19 = Insufficient ETH value in message.<br>
20 = Global Mint is locked.<br>
21 = Invalid address trying to update attributes.<br>
22 = NFT does not belong to you.<br>
23 = You can't equip items onto NFTs that don't belong to you.<br>
24 = You can't equip this item yet.<br>
25 = Already equipping an item in this slot.<br>
26 = The hero's class cannot wear this equipment.<br>
27 = Invalid address initializing durability.<br>
28 = Invalid address decreasing durability.<br>
29 = Equipment already burnt.<br>
30 = Tier Up not enabled.<br>
31 = Must exceed 0.<br>
32 = Exceeded Maximum Supply.<br>
33 = Invalid address trying to mint tokens.<br>
34 = Cannot mint to 0 address.<br>
35 = Invalid address changing naming fee.<br>
36 = Invalid address initializing name.<br>
37 = Invalid signature.<br>
38 = Message sender mismatch.<br>
39 = Exceeded maximum individual mints.<br>

B1 = Hero bridged does not belong to you.

SFF = Safe transfer failed. Not approved nor owner.

E1 = Can't sell while anti-snipe timer is activated.<br>
E2 = Recipient/sender is a blacklisted address.<br>
EZA = Cannot transfer from 0 address.<br>
EZB = Value exceeds user balance.<br>
EZBA = Value exceeds user allowance.<br>
EZC = Insufficient allowance.<br>
EZD = Cannot approve from zero address.<br>
EZE = Cannot approve to zero address.<br>
EZF = Approval failed.<br>

LPNE = Trying to claim within lock period.<br>
LPB0 = No balance to unstake.<br>
LPA0 = Invalid stake amount.
