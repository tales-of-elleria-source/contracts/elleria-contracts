pragma solidity ^0.8.0;
//SPDX-License-Identifier: MIT

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "./interfaces/IVRFHelper.sol";
import "./interfaces/IEllerianHero.sol";
import "./interfaces/ISignature.sol";
import "./interfaces/IHeroBridge.sol";

/// @title EllerianHeroUpgradeable
/// @author Wayne (Ellerian Prince)
/// @dev Contract for managing Ellerian heroes, their upgrade costs, attributes, levels, and associated fees.
///     This contract will be deprecated over time- source of truth will come from Elleria's backend.
///     This allows game balancing and enables custom in-game mechanics.
///     Reliability of the backend to be guaranteed by Chainlink's VRF.
/// @notice The contract is ownable. The owner can relink contract/address references and change EXP requirements. 
contract EllerianHeroUpgradeable is Ownable {
  using SafeERC20 for IERC20;

  /// @notice Fail probabilities for each upgrade level. Each index represents a level with its corresponding probability.
  uint256[] public fail_probability = [0, 0, 0, 0, 10, 20, 25, 30, 35, 40, 50, 60, 60, 60, 60];

  /// @notice Gold cost for each upgrade level. Each index represents a level with its corresponding cost.
  uint256[] public upgrade_gold_cost = [0, 20000, 50000, 150000, 450000, 1000000, 2000000, 5000000, 10000000, 20000000, 50000000, 100000000, 100000000, 100000000, 100000000];

  /// @notice Token cost for each upgrade level. Each index represents a level with its corresponding cost.
  uint256[] public upgrade_token_cost = [0, 0, 0, 0, 5, 50, 100, 500, 1000, 1000, 2000, 5000, 5000, 5000, 10000];

  /// @notice Experience required each level for upgrades. Each index represents a level with its corresponding required experience.
  uint256[] public experience_needed = [0, 0, 50, 150, 450, 1000, 2000, 3000, 4000, 8000, 10000, 12000, 14000, 16000, 50000];

  /// @notice Represents the rarity thresholds for hero attributes.
  uint256[] public attributesRarity = [0, 250, 320];

  /// @notice Mapping from token ID to hero's experience points.
  mapping(uint256 => uint256) public heroExperience;

  /// @notice Mapping from token ID to hero's level.
  mapping(uint256 => uint256) public heroLevel;

  /// @notice Mapping from token ID to hero's name.
  mapping(uint256 => string) public heroName;

  /// @dev Mapping from token ID to whether the hero is staked or not.
  mapping (uint256 => bool) private isStaked;

  /// @notice Cost for changing the name of a hero.
  uint256 public nameChangeFee = (5000 * 10 * 18);

  /// @notice Mapping from address to whether the address is approved to update states.
  mapping (address => bool) public _approvedAddresses;
  
  /// @dev Structure defining the attributes of a hero.
  struct heroAttributes {
    uint256 str;          // Strength =     PHYSICAL ATK
    uint256 agi;          // Agility =      HIT RATE
    uint256 vit;          // Vitality =     HEALTH
    uint256 end;          // Endurance =    PHYSICAL DEFENSE
    uint256 intel;        // Intelligence = MAGIC ATK
    uint256 will;         // Will =         MAGIC DEFENSE
    uint256 total;        // Total Attributes 
    uint256 class;        // Class
    uint256 tokenid;      // Token ID
    uint256 summonedTime; // Time of Mint
  }

  /// @notice Mapping from token ID to hero's attributes.
  mapping(uint256 => heroAttributes) public heroDetails;

  /// @notice Address of the ERC721 Hero NFT.
  address public minterAddress;

  /// @notice Address of the $ELM-equivalent ERC20 required for upgrades.
  address public tokenAddress;

  /// @notice Address of the $MEDALS-equivalent ERC20 required for upgrades.
  address public goldAddress;

  /// @notice Address fees will be sent to.
  address public feesAddress;

  /// @notice Address used to verify signatures.
  address public signerAddr;

  /// @dev Reference to the contract that allows staking/unstaking.
  IHeroBridge private heroBridgeAbi;

  /// @dev Reference to the contract that does signature verifications.
  ISignature private signatureAbi;

  /// @dev Reference to the contract that does randomizing.
  IVRFHelper vrfAbi;
  

  /// @dev Allows approved addresses to set attribute rarity thresholds.
  /// @param _attributes An array containing the rarity thresholds.
  function SetAttributeRarity(uint256[] memory _attributes) external {
    require(_approvedAddresses[msg.sender], "17");

    attributesRarity = _attributes;

    emit AttributeRarityChange(msg.sender, _attributes);
  }

    
  /// @dev Allows approved addresses to update the name change fee.
  /// @param _feeInWEI The fee (in WEI) to change name.
  function SetNameChangeFee(uint256 _feeInWEI) external {
    require(_approvedAddresses[msg.sender], "17");
    nameChangeFee = _feeInWEI;

    emit RenamingFeeChange(msg.sender, _feeInWEI);
  }

  /// @dev Allows the owner to update address references.
  /// @param _minterAddress The address of the contract for the ERC721 heroes.
  /// @param _goldAddress The address of the contract for the ERC20 $MEDALS-equivalent.
  /// @param _tokenAddress The address of the contract for the ERC20 $ELM-equivalent.
  /// @param _signatureAddr The address of the contract for the signature verifier.
  /// @param _feesAddress The address collecting fees.
  /// @param _vrfAddress The address of the contract to generate randomized numbers.
  /// @param _signerAddr The address of the private signer.
  /// @param _heroBridgeAddr The address of the contract handling hero bridging.
  function SetAddresses(address _minterAddress, address _goldAddress,  address _tokenAddress, 
  address _signatureAddr, address _feesAddress, address _vrfAddress, address _signerAddr,
  address _heroBridgeAddr) external onlyOwner {
      minterAddress = _minterAddress;
      goldAddress = _goldAddress;
      tokenAddress = _tokenAddress;
      feesAddress = _feesAddress;
      signerAddr = _signerAddr;

      heroBridgeAbi = IHeroBridge(_heroBridgeAddr);
      signatureAbi = ISignature(_signatureAddr);
      vrfAbi = IVRFHelper(_vrfAddress);
  }


  /// @dev Delegate admin addresses. Sender must be contract owner.
  /// @param _address The address to change permissions for.
  /// @param _allowed Is this address an admin?
  function SetApprovedAddress(address _address, bool _allowed) public onlyOwner {
      _approvedAddresses[_address] = _allowed;
  }

  /// @dev Allows the owner to update upgrade costs.
  /// @param _new_gold_costs_in_ether An array containing the $MEDALS upgrade costs (in Ether)
  /// @param _new_token_costs_in_ether An array containing the $ELM upgrade costs (in Ether)
  /// @param _new_chances An array containing the fail rates (0-100 representing %).
  function SetUpgradeRequirements(uint256[] memory _new_gold_costs_in_ether, uint256[] memory _new_token_costs_in_ether, uint256[] memory _new_chances) external onlyOwner {
    upgrade_gold_cost = _new_gold_costs_in_ether;
    upgrade_token_cost = _new_token_costs_in_ether;
    fail_probability = _new_chances;

    emit UpgradeRequirementsChanged(msg.sender, _new_gold_costs_in_ether, _new_token_costs_in_ether, _new_chances);
  }

  /// @dev Allows the owner to update EXP requirements for upgrades.
  /// @param _new_exp An array containing the upgrade EXP requirements.
  function SetEXPRequiredForLevelUp(uint256[] memory _new_exp) external onlyOwner {
    experience_needed = _new_exp;
    emit ExpRequirementsChanged(msg.sender, _new_exp);
  }

  /// @notice Sets a new name for a hero. Sender must be the token's owner.
  /// @param _tokenId The ID of the hero to rename.
  /// @param _name The new name for the hero.
  function SetHeroName(uint256 _tokenId, string memory _name) public {
    require(IERC721(minterAddress).ownerOf(_tokenId) == tx.origin, "22");
    heroName[_tokenId] = _name;

    IERC20(goldAddress).safeTransferFrom(tx.origin, feesAddress, nameChangeFee);
    emit NameChange(msg.sender, _tokenId, _name);
  }

  /// @dev Updates the attributes of a hero. Sender must be an approved address.
  /// @param _tokenId The ID of the hero to update.
  /// @param _str The new strength of the hero.
  /// @param _agi The new agility of the hero.
  /// @param _vit The new vitality of the hero.
  /// @param _end The new endurance of the hero.
  /// @param _intel The new intelligence of the hero.
  /// @param _will The new willpower of the hero.
  function UpdateAttributes(uint256 _tokenId, uint256 _str, uint256 _agi, uint256 _vit, uint256 _end, uint256 _intel, uint256 _will) external {
    require(_approvedAddresses[msg.sender], "21");

    heroDetails[_tokenId].str = _str;
    heroDetails[_tokenId].agi = _agi;
    heroDetails[_tokenId].vit = _vit;
    heroDetails[_tokenId].end = _end;
    heroDetails[_tokenId].intel = _intel;
    heroDetails[_tokenId].will = _will;
    heroDetails[_tokenId].total = _str + _agi + _vit + _end + _intel + _will;

    uint256 class = heroDetails[_tokenId].class;

    emit AttributeChange(msg.sender, _tokenId, _str, _agi, _vit, _end, _intel, _will, class);
  }

  /// @notice Gets if a hero is staked. Staked heroes can perform in-game actions but cannot be moved.
  /// @param _tokenId The ID of the hero to check.
  /// @return true if the hero is staked, false otherwise.
  function IsStaked(uint256 _tokenId) external view returns (bool) {
      return isStaked[_tokenId];
  }


  /// @notice Gets the detailed attributes of a specific hero.
  /// @param _tokenId The ID of the hero to query.
  /// @return An array containing the hero's strength, agility, vitality, endurance, intelligence, willpower, total attributes, class and summon time.
  function GetHeroDetails(uint256 _tokenId) external view returns (uint256[9] memory) {
    return [heroDetails[_tokenId].str, heroDetails[_tokenId].agi, heroDetails[_tokenId].vit, 
    heroDetails[_tokenId].end, heroDetails[_tokenId].intel, heroDetails[_tokenId].will, 
    heroDetails[_tokenId].total, heroDetails[_tokenId].class, heroDetails[_tokenId].summonedTime];
  }

  /// @notice Gets the class for a specific hero.
  /// @param _tokenId The token ID of the hero
  /// @return Returns the class ID of the hero
  function GetHeroClass(uint256 _tokenId) external view returns (uint256) {
    return heroDetails[_tokenId].class;
  }

  /// @notice Gets the level for a specific hero.
  /// @param _tokenId The token ID of the hero
  /// @return Returns the level of the hero
  function GetHeroLevel(uint256 _tokenId) external view returns (uint256) {
    return heroLevel[_tokenId];
  }
  
  /// @notice Gets the upgrade cost for a specific level.
  /// @param _level The current level of the hero
  /// @return Returns the upgrade gold and token cost for the next level
  function GetUpgradeCost(uint256 _level) external view returns (uint256[2] memory) {
    return [upgrade_gold_cost[_level], upgrade_token_cost[_level]];
  }

  /// @notice Gets the upgrade cost for a specific hero.
  /// @param _tokenId The token ID of the hero
  /// @return Returns the upgrade gold and token cost for the next level
  function GetUpgradeCostFromTokenId(uint256 _tokenId) public view returns (uint256[2] memory) {
    return [upgrade_gold_cost[heroLevel[_tokenId]], upgrade_token_cost[heroLevel[_tokenId]]];
  }

  /// @notice Gets the experience a certain hero has.
  /// @param _tokenId The token ID of the hero
  /// @return Returns the current experience and the experience needed for the next level
  function GetHeroExperience(uint256 _tokenId) external view returns (uint256[2] memory) {
    return [heroExperience[_tokenId], experience_needed[heroLevel[_tokenId]]];
  }
  
  /// @dev Resets a hero's experience to its starting point. Sender must be an approved address.
  /// @param _tokenId The token ID of the hero
  /// @param _exp The new experience of the hero
  function ResetHeroExperience(uint256 _tokenId, uint256 _exp) external {
      require(_approvedAddresses[msg.sender], "18");
      heroExperience[_tokenId] = _exp;

      emit ExpChange(msg.sender, _tokenId, _exp);
  }

  /// @notice Gets the name for a specific hero.
  /// @param _tokenId The token ID of the hero
  /// @return Returns the name of the hero
  function GetHeroName(uint256 _tokenId) external view returns (string memory) {
    return heroName[_tokenId];
  }

  /// @notice Gets the exp required to upgrade for a certain level.
  /// @param _level The current level of the hero
  /// @return Returns the experience needed for the next level
  function GetEXPRequiredForLevelUp(uint256 _level) external view returns (uint256) {
    return experience_needed[_level];
  }

  /// @notice Gets the probability to fail an upgrade for a certain level.
  /// @param _level The current level of the hero
  /// @return Returns the failure probability for the next level
  function GetFailProbability(uint256 _level) external view returns (uint256) {
    return fail_probability[_level];
  }

  /// @notice Gets the attribute rarity of a hero.
  /// @param _tokenId The token ID of the hero
  /// @return Returns the attribute rarity
  function GetAttributeRarity(uint256 _tokenId) external view returns (uint256) {
    uint256 _totalAttributes = heroDetails[_tokenId].total;

    if (_totalAttributes < attributesRarity[1])
      return 0;
    else if (_totalAttributes < attributesRarity[2])
      return 1;

    return 2;
  }

  /// @dev Rewards a certain hero with experience. Sender must be an approved address.
  /// @param _tokenId The token ID of the hero
  /// @param _exp The amount of experience to add
  function UpdateHeroExperience(uint256 _tokenId, uint256 _exp) external {
    require(_approvedAddresses[msg.sender], "7");
    heroExperience[_tokenId] += _exp;

    emit ExpChange(msg.sender, _tokenId, heroExperience[_tokenId]);
  }

  /// @dev Sets the level for a specific hero. Sender must be an approved address.
  /// @param _tokenId The token ID of the hero
  /// @param _level The new level of the hero
  function SetHeroLevel (uint256 _tokenId, uint256 _level) external {
    require(_approvedAddresses[msg.sender], "17");
    heroLevel[_tokenId] = _level;

    emit LevelChange(msg.sender, _tokenId, _level, 0, 0);
  }

  /// @dev Function to synchronize a hero's stats from Elleria onto the blockchain. Sender must be the token's owner.
  /// @param _signature The signature of the data to synchronize.
  /// @param _data An array containing the hero's token ID, strength, agility, vitality, endurance, intelligence, willpower, total attributes, class, level, and experience.
  function SynchronizeHero (bytes memory _signature, uint256[] memory _data) external {
    require (heroBridgeAbi.GetOwnerOfTokenId(_data[0]) == msg.sender, "Not owner");
    require(_data.length == 1, "Invalid Data Length");
    // _data[0] = tokenId, [1] = str, [2] = agi, [3] = vit, [4] = end, [5] = int, [6] = will, [7] = total, [8] = class, [9] = level,
    // [10] = experience
    uint256 tokenId = _data[0];

    heroDetails[tokenId].str = _data[1];
    heroDetails[tokenId].agi = _data[2];
    heroDetails[tokenId].vit = _data[3];
    heroDetails[tokenId].end = _data[4];
    heroDetails[tokenId].intel = _data[5];
    heroDetails[tokenId].will = _data[6];
    heroDetails[tokenId].total = _data[7];
    heroDetails[tokenId].class = _data[8];

    heroLevel[tokenId] = _data[9];
    heroExperience[tokenId] = _data[10];

    emit AttributeChange(msg.sender, tokenId, _data[1], _data[2], _data[3], _data[4], _data[5],  _data[6], _data[8]);
    emit ExpChange(msg.sender, tokenId, _data[10]);
    emit LevelChange(msg.sender, tokenId, _data[9], 0, 0);
    require(signatureAbi.bigVerify(signerAddr, msg.sender, _data, _signature));
  }

  /// @dev Function to attempt an upgrade on a hero. Sender must be the owner of the token.
  /// @param _tokenId The ID of the hero to upgrade.
  /// @param _goldAmountInEther The amount of gold (in Ether) to spend on the upgrade.
  /// @param _tokenAmountInEther The amount of tokens (in Ether) to spend on the upgrade.
  function AttemptHeroUpgrade(uint256 _tokenId, uint256 _goldAmountInEther, uint256 _tokenAmountInEther) public {
      require (IERC721(minterAddress).ownerOf(_tokenId) == tx.origin, "22");
      require(msg.sender == tx.origin, "9");

      // Transfer $MEDALS
      uint256 correctAmount = _goldAmountInEther * 1e18;
      IERC20(goldAddress).safeTransferFrom(tx.origin, feesAddress, correctAmount);

      // Transfer $ELLERIUM
      correctAmount = _tokenAmountInEther * 1e18;
      if (correctAmount > 0) {
      IERC20(tokenAddress).safeTransferFrom(tx.origin, feesAddress, correctAmount);
      }

      // Attempts the upgrade.
      uint256 level = updateLevel(_tokenId, _goldAmountInEther, _tokenAmountInEther);

      emit Upgrade(msg.sender, _tokenId, level);
  }

  /// @dev Rolls a number to check if the upgrade succeeds.
  /// @param _tokenId The token ID of the hero
  /// @param _goldAmountInEther The amount of gold provided for the upgrade
  /// @param _tokenAmountInEther The amount of tokens provided for the upgrade
  /// @return Returns the new level of the hero
  function updateLevel(uint256 _tokenId, uint256 _goldAmountInEther, uint256 _tokenAmountInEther) internal returns (uint256) {
      uint256[2] memory UpgradeCosts = GetUpgradeCostFromTokenId(_tokenId);
      require(_goldAmountInEther == UpgradeCosts[0], "ERR4");
      require(_tokenAmountInEther == UpgradeCosts[1], "ERR5");
      require(heroExperience[_tokenId] >= experience_needed[heroLevel[_tokenId]], "ERR6");

      uint256 randomNumber = vrfAbi.GetVRF(_tokenId) % 100;
      if (randomNumber >= fail_probability[heroLevel[_tokenId]]) {
        heroLevel[_tokenId] += 1;
        emit LevelChange(msg.sender, _tokenId, heroLevel[_tokenId], _goldAmountInEther, _tokenAmountInEther);
      }

      return heroLevel[_tokenId];
  }

  /// @dev Function to initialize a hero upon minting. Sender must be an approved addresses.
  /// @param _tokenId The ID of the hero to initialize.
  /// @param _str The initial strength of the hero.
  /// @param _agi The initial agility of the hero.
  /// @param _vit The initial vitality of the hero.
  /// @param _end The initial endurance of the hero.
  /// @param _intel The initial intelligence of the hero.
  /// @param _will The initial willpower of the hero.
  /// @param _total The initial total attributes of the hero.
  /// @param _class The initial class of the hero.
  function initHero(uint256 _tokenId, uint256 _str, uint256 _agi, uint256 _vit, uint256 _end,
  uint256 _intel, uint256 _will, uint256 _total, uint256 _class) external {
    require(_approvedAddresses[msg.sender], "36");
    heroName[_tokenId] = "Hero";
    heroLevel[_tokenId] = 1;

    heroDetails[_tokenId] = heroAttributes({
      str: _str,
      agi: _agi, 
      vit: _vit,
      end: _end,
      intel: _intel,
      will: _will,
      total: _total,
      class: _class,
      tokenid: _tokenId,
      summonedTime: block.timestamp
      });

    emit AttributeChange(msg.sender, _tokenId, _str, _agi, _vit, _end, _intel, _will, _class);
  }

  /// @notice Locks the hero temporarily so he can perform in-game actions.
  /// @param _tokenId The token ID of the hero
  function Stake(uint256 _tokenId) external {
    require(_approvedAddresses[msg.sender], "36");
    isStaked[_tokenId] = true;
    emit Staked(_tokenId);
  }

  /// @notice Unlocks the hero.
  /// @param _tokenId The token ID of the hero
  function Unstake(uint256 _tokenId) external {
    require(_approvedAddresses[msg.sender], "36");
    isStaked[_tokenId] = false;
    emit Unstaked(_tokenId);
  }

  /// @notice Event emitted when a hero is staked.
  /// @param tokenId The token ID of the staked hero
  event Staked(uint256 tokenId);

  /// @notice Event emitted when a hero is unstaked.
  /// @param tokenId The token ID of the unstaked hero
  event Unstaked(uint256 tokenId);

  /// @notice Event emitted when the attribute rarity changes.
  /// @param _from The address of the contract caller
  /// @param _newRarity The new rarity values
  event AttributeRarityChange(address _from, uint256[] _newRarity);

  /// @notice Event emitted when the renaming fee changes.
  /// @param _from The address of the contract caller 
  /// @param _newFee The new fee value (in WEI)
  event RenamingFeeChange(address _from, uint256 _newFee);

  /// @notice Event emitted when the upgrade requirements change.
  /// @param _from The address of the contract caller
  /// @param _new_gold_costs_in_ether The new gold cost values
  /// @param _new_token_costs_in_ether The new token cost values
  /// @param _new_chances The new chances values
  event UpgradeRequirementsChanged(address _from, uint256[] _new_gold_costs_in_ether, uint256[] _new_token_costs_in_ether, uint256[] _new_chances);

  /// @notice Event emitted when the experience requirements change.
  /// @param _from The address of the contract caller
  /// @param _expRequirements The new experience requirements
  event ExpRequirementsChanged(address _from, uint256[] _expRequirements);

  /// @notice Event emitted when a hero's name changes.
  /// @param _from The address of the contract caller
  /// @param _tokenId The token ID of the hero whose name changed
  /// @param _newName The new name of the hero
  event NameChange(address _from, uint256 _tokenId, string _newName);

  /// @notice Event emitted when a hero is upgraded.
  /// @param _from The address of the contract caller
  /// @param _tokenId The token ID of the upgraded hero
  /// @param _level The new level of the hero
  event Upgrade(address _from, uint256 _tokenId, uint256 _level);

  /// @notice Event emitted when a hero's experience changes.
  /// @param _from The address of the contract caller
  /// @param _tokenId The token ID of the hero whose experience changed
  /// @param _exp The new experience of the hero
  event ExpChange(address _from, uint256 _tokenId, uint256 _exp);

  /// @notice Event emitted when a hero's level changes.
  /// @param _from The address of the contract caller
  /// @param _tokenId The token ID of the hero whose level changed
  /// @param _level The new level of the hero
  /// @param _gold The gold cost for the level change
  /// @param _token The token cost for the level change
  event LevelChange(address _from, uint256 _tokenId, uint256 _level, uint256 _gold, uint256 _token);

  /// @param _agi The new agility attribute
  /// @param _vit The new vitality attribute
  /// @param _end The new endurance attribute
  /// @param _intel The new intelligence attribute
  /// @param _will The new willpower attribute
  /// @param _class The new class attribute
  event AttributeChange(address _from, uint256 _tokenId, uint256 _str, uint256 _agi, uint256 _vit, uint256 _end, uint256 _intel, uint256 _will, uint256 _class);
}