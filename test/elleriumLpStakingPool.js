const ElleriumLPStakingPool = artifacts.require("ElleriumLPStakingPool");
const SimpleToken = artifacts.require("SimpleToken");
const ElmToken = artifacts.require("ElleriumTokenERC20v2");

const BN = require('bn.js');
const { advanceTime, wait, mineBlock, setBlockTime } = require('./helpers/index');

contract('ElleriumLPStakingPool', (accounts) => {

    const contractsOwner = accounts[0];
    let stakingFeeAddress;
    const userAddress = accounts[2];
    const othersAddress = accounts[3]; // Simulates having additional balances in the pool not owned by user.
    const emptyAddress = accounts[4];
    let lpContractInstance;
    let elmContractInstance;
    let mlpContractInstance;

    before(async () => {
        // Deploy placeholder $ELM and give the LP contract mint capabilities.
        elmContractInstance = await ElmToken.new();
        
        // Deploy placeholder $MLP and give user 500 $MLP.
        mlpContractInstance = await SimpleToken.new("MLP", "MLP", BigInt(50000 * 1e18));
        await mlpContractInstance.transfer(userAddress, BigInt(500 * 1e18), {from: contractsOwner});
        await mlpContractInstance.transfer(othersAddress, BigInt(500 * 1e18), {from: contractsOwner});
        
        const stakingToken = mlpContractInstance.address;
        const rewardsToken = elmContractInstance.address;
        lpContractInstance = await ElleriumLPStakingPool.new(stakingToken, rewardsToken);

        stakingFeeAddress = await lpContractInstance.stakingLPFeesAddress();

        // Approve the LP to mint.
        await elmContractInstance.setApprovedAddress(lpContractInstance.address, true, {from: contractsOwner});
            
        // Fix block time to standardize results.
        await setBlockTime(1773972772, contractsOwner);
        // const currentTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(currentTimestamp);
    });

    beforeEach(async () => {
        await mineBlock(contractsOwner);
        await wait(1000);
    })

    it('should set reward rate correctly for owner', async () => {
        const emissionRate = BigInt(5787036000000000);
        const isFeesApply = true;
        
        await lpContractInstance.setRewardRate(emissionRate, isFeesApply, {from: contractsOwner});
        
        const rewardRateSet = await lpContractInstance.rewardRate();
        const isFeesApplySet = await lpContractInstance.applyFees();
        
        assert.equal(rewardRateSet.toString(), emissionRate.toString(), "The reward rate is not set correctly");
        assert.equal(isFeesApplySet, isFeesApply, "The apply fees flag is not set correctly");
    });
    
    it('should not set reward rate if not owner', async () => {
        const emissionRate = BigInt(5787036000000000);
        const isFeesApply = true;

        try {
            await lpContractInstance.setRewardRate(emissionRate, isFeesApply, {from: userAddress});
            assert.fail("The transaction did not fail as expected");
        } catch (error) {
            assert.ok(error.toString().includes('revert'), "The error message should contain 'revert'");
        }
    });

    it('should stake correctly for others', async () => {
        const initialUserBalance = await mlpContractInstance.balanceOf(othersAddress);
        const stakeAmount = new BN(BigInt(100 * 1e18));
        const expectedEndAmount = initialUserBalance.sub(stakeAmount);

        // Approve
        await mlpContractInstance.approve(lpContractInstance.address, stakeAmount.toString(), {from: othersAddress});
        const allowance = await mlpContractInstance.allowance(othersAddress, lpContractInstance.address);
        assert.equal(allowance.toString(), stakeAmount.toString(), "Allowance is incorrect");

        // Stake
        await lpContractInstance.stake(stakeAmount.toString(), {from: othersAddress});
        
        // Verify
        const balance = await lpContractInstance.balanceOf(othersAddress);
        assert.equal(balance.toString(), stakeAmount.toString(), "The staking amount is incorrect");

        const userBalance = await mlpContractInstance.balanceOf(othersAddress);
        assert.equal(userBalance.toString(), expectedEndAmount.toString(), "The deducted amount is incorrect");
    });

    it('should stake correctly for user', async () => {
        const initialUserBalance = await mlpContractInstance.balanceOf(userAddress);
        const stakeAmount = new BN(BigInt(100 * 1e18));
        const expectedEndAmount = initialUserBalance.sub(stakeAmount);

        // Approve
        await mlpContractInstance.approve(lpContractInstance.address, stakeAmount.toString(), {from: userAddress});
        const allowance = await mlpContractInstance.allowance(userAddress, lpContractInstance.address);
        assert.equal(allowance.toString(), stakeAmount.toString(), "Allowance is incorrect");

        // Stake
        await lpContractInstance.stake(stakeAmount.toString(), {from: userAddress});
        
        // Verify
        const balance = await lpContractInstance.balanceOf(userAddress);
        assert.equal(balance.toString(), stakeAmount.toString(), "The staking amount is incorrect");

        const userBalance = await mlpContractInstance.balanceOf(userAddress);
        assert.equal(userBalance.toString(), expectedEndAmount.toString(), "The deducted amount is incorrect");
    });

    it('should not allow stakes of less than 1 Ether', async () => {
        const amount = BigInt(5000);

        try {
            await lpContractInstance.stake(amount, {from: userAddress});
            assert.fail("The transaction did not fail as expected");
        } catch (error) {
            assert.ok(error.toString().includes('revert'), "The error message should contain 'revert'");
        }
    });

    it('should unstake correctly with slashing fee', async () => {   
        const initialBalance = await lpContractInstance.balanceOf(userAddress);
        assert.equal(initialBalance, BigInt(100 * 1e18), "The staked amount is incorrect")

        // Check withdrawal fee
        const withdrawalFees = await lpContractInstance.getWithdrawalFees({from: userAddress});
        assert.equal(withdrawalFees.toString(), '5000');
     
        const unstakeAmount = new BN(BigInt(1 * 1e18));
        await lpContractInstance.unstake(unstakeAmount.toString(), {from: userAddress});
        
        const finalBalance = await lpContractInstance.balanceOf(userAddress);
        assert.equal(finalBalance.toString(), initialBalance.sub(unstakeAmount).toString(), "Balance in LP wasn't deducted properly");

        const expectedFee = BigInt('500000000000000000');

        // Validate fee taken. User balance is validated at after all deductions.
        const feesBalance = await mlpContractInstance.balanceOf(stakingFeeAddress);
        assert.equal(feesBalance, expectedFee, "Incorrect fee allocated");
    });

    it('should unstake correctly with < 1 hour fee', async () => {
        // Advance the block timestamp by 30 minutes.
        // const currentTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(currentTimestamp);

        await advanceTime(30 * 60, contractsOwner) // Advance time by 30 minutes.

        // const updatedTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(updatedTimestamp);
        
        // Check withdrawal fee
        const withdrawalFees = await lpContractInstance.getWithdrawalFees({from: userAddress});
        assert.equal(withdrawalFees.toString(), '2000');

        const initialBalance = await lpContractInstance.balanceOf(userAddress);

        const unstakeAmount = new BN(BigInt(1 * 1e18));      
        await lpContractInstance.unstake(unstakeAmount.toString(), {from: userAddress});
        
        const finalBalance = await lpContractInstance.balanceOf(userAddress);
        assert.equal(finalBalance.toString(), initialBalance.sub(unstakeAmount).toString(), "Balance in LP wasn't deducted properly");

        // Validate fee taken (acccumualtive of previous tests). 
        // User balance is validated at after all deductions.
        const expectedFee = BigInt('700000000000000000');
        const feesBalance = await mlpContractInstance.balanceOf(stakingFeeAddress);
        assert.equal(feesBalance, expectedFee, "Incorrect fee allocated");
    });

    it('should have accumulated correct rewards value up till 1h for user', async () => {
        // These test the updateReward modifier as well since unstake triggers the update.
        const currentTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        await setBlockTime(1773974582, contractsOwner);

        const updatedTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        assert.ok(+updatedTimestamp >= 1773974582 && +updatedTimestamp <= 1773974582, "Invalid timestamp");

        const pendingRewards = await lpContractInstance.checkTotalRewards({from: userAddress});
        console.log(pendingRewards.toString());
        assert.ok(pendingRewards.gte(
            new BN("5182100000000000000")) && pendingRewards.lte(new BN("5196600000000000000")),
        "Invalid reward");
    })

    it('should unstake correctly with < 1 day fee ', async () => {
        // Advance the block timestamp by 30 minutes.
        // const currentTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(currentTimestamp);

        await advanceTime(60 * 60 * 23, contractsOwner) // Advance time by 23 hours.

        // const updatedTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(updatedTimestamp);

        // Check withdrawal fee
        const withdrawalFees = await lpContractInstance.getWithdrawalFees({from: userAddress});
        assert.equal(withdrawalFees.toString(), '1000');

        const initialBalance = await lpContractInstance.balanceOf(userAddress);

        const unstakeAmount = new BN(BigInt(1 * 1e18));      
        await lpContractInstance.unstake(unstakeAmount.toString(), {from: userAddress});
        
        const finalBalance = await lpContractInstance.balanceOf(userAddress);
        assert.equal(finalBalance.toString(), initialBalance.sub(unstakeAmount).toString(), "Balance in LP wasn't deducted properly");

        // Validate fee taken (acccumualtive of previous tests). 
        // User balance is validated at after all deductions.
        const expectedFee = BigInt('800000000000000000');
        const feesBalance = await mlpContractInstance.balanceOf(stakingFeeAddress);
        assert.equal(feesBalance, expectedFee, "Incorrect fee allocated");
    });

    it('should unstake correctly with < 3 day fee ', async () => {
        // Advance the block timestamp by 30 minutes.
        // const currentTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(currentTimestamp);

        await advanceTime(60 * 60 * 24 * 2, contractsOwner) // Advance time by 2 days.

        // const updatedTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(updatedTimestamp);

        // Check withdrawal fee
        const withdrawalFees = await lpContractInstance.getWithdrawalFees({from: userAddress});
        assert.equal(withdrawalFees.toString(), '500');

        const initialBalance = await lpContractInstance.balanceOf(userAddress);

        const unstakeAmount = new BN(BigInt(1 * 1e18));      
        await lpContractInstance.unstake(unstakeAmount.toString(), {from: userAddress});
        
        const finalBalance = await lpContractInstance.balanceOf(userAddress);
        assert.equal(finalBalance.toString(), initialBalance.sub(unstakeAmount).toString(), "Balance in LP wasn't deducted properly");

        // Validate fee taken (acccumualtive of previous tests). 
        // User balance is validated at after all deductions.
        const expectedFee = BigInt('850000000000000000');
        const feesBalance = await mlpContractInstance.balanceOf(stakingFeeAddress);
        assert.equal(feesBalance, expectedFee, "Incorrect fee allocated");
    });

    it('should have accumulated correct rewards value up till 3d for user', async () => {
        const currentTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        await setBlockTime(1774230185, contractsOwner);

        const updatedTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        assert.ok(+updatedTimestamp >= 1774230185 && +updatedTimestamp <= 1774230185, "Invalid timestamp");

        const pendingRewards = await lpContractInstance.checkTotalRewards({from: userAddress});
        console.log(pendingRewards.toString());
        assert.ok(
            pendingRewards.gt(new BN("734730000000000000000")) && pendingRewards.lt(new BN("734760000000000000000"))
        , "Invalid reward");
    })

    it('should unstake correctly with < 7 days fee ', async () => {
        // Advance the block timestamp by 30 minutes.
        // const currentTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(currentTimestamp);

        await advanceTime(60 * 60 * 24 * 3, contractsOwner) // Advance time by 3 days.

        // const updatedTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(updatedTimestamp);

        // Check withdrawal fee
        const withdrawalFees = await lpContractInstance.getWithdrawalFees({from: userAddress});
        assert.equal(withdrawalFees.toString(), '300');

        const initialBalance = await lpContractInstance.balanceOf(userAddress);

        const unstakeAmount = new BN(BigInt(1 * 1e18));      
        await lpContractInstance.unstake(unstakeAmount.toString(), {from: userAddress});
        
        const finalBalance = await lpContractInstance.balanceOf(userAddress);
        assert.equal(finalBalance.toString(), initialBalance.sub(unstakeAmount).toString(), "Balance in LP wasn't deducted properly");

        // Validate fee taken (acccumualtive of previous tests). 
        // User balance is validated at after all deductions.
        const expectedFee = BigInt('880000000000000000');
        const feesBalance = await mlpContractInstance.balanceOf(stakingFeeAddress);
        assert.equal(feesBalance, expectedFee, "Incorrect fee allocated");
    });

    it('should unstake correctly with < 30 days fee ', async () => {
        // Advance the block timestamp by 30 minutes.
        // const currentTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(currentTimestamp);

        await advanceTime(60 * 60 * 24 * 20, contractsOwner) // Advance time by 20 days.

        // const updatedTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(updatedTimestamp);

        // Check withdrawal fee
        const withdrawalFees = await lpContractInstance.getWithdrawalFees({from: userAddress});
        assert.equal(withdrawalFees.toString(), '100');

        const initialBalance = await lpContractInstance.balanceOf(userAddress);

        const unstakeAmount = new BN(BigInt(1 * 1e18));      
        await lpContractInstance.unstake(unstakeAmount.toString(), {from: userAddress});
        
        const finalBalance = await lpContractInstance.balanceOf(userAddress);
        assert.equal(finalBalance.toString(), initialBalance.sub(unstakeAmount).toString(), "Balance in LP wasn't deducted properly");

        // Validate fee taken (acccumualtive of previous tests). 
        // User balance is validated at after all deductions.
        const expectedFee = BigInt('890000000000000000');
        const feesBalance = await mlpContractInstance.balanceOf(stakingFeeAddress);
        assert.equal(feesBalance, expectedFee, "Incorrect fee allocated");
    });

    it('should have accumulated correct rewards value up till 30d for user', async () => {
        const currentTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        await setBlockTime(1776217388, contractsOwner);

        const updatedTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        assert.ok(+updatedTimestamp >= 1776217388 && +updatedTimestamp <= 1776217388, "Invalid timestamp");

        const pendingRewards = await lpContractInstance.checkTotalRewards({from: userAddress});
        console.log(pendingRewards.toString());
        assert.ok(
            pendingRewards.gt(new BN("6341200000000000000000")) && pendingRewards.lt(new BN("6341300000000000000000"))
        , "Invalid reward");
    })

    it('should unstake correctly with most lenient fee ', async () => {
        // Advance the block timestamp by 30 minutes.
        // const currentTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(currentTimestamp);

        await advanceTime(60 * 60 * 24 * 20, contractsOwner) // Advance time by 20 days.

        // const updatedTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        // console.log(updatedTimestamp);

        // Check withdrawal fee
        const withdrawalFees = await lpContractInstance.getWithdrawalFees({from: userAddress});
        assert.equal(withdrawalFees.toString(), '50');

        const initialBalance = await lpContractInstance.balanceOf(userAddress);

        const unstakeAmount = new BN(BigInt(1 * 1e18));      
        await lpContractInstance.unstake(unstakeAmount.toString(), {from: userAddress});
        
        const finalBalance = await lpContractInstance.balanceOf(userAddress);
        assert.equal(finalBalance.toString(), initialBalance.sub(unstakeAmount).toString(), "Balance in LP wasn't deducted properly");

        // Validate fee taken (acccumualtive of previous tests). 
        // User balance is validated at after all deductions.
        const expectedFee = BigInt('895000000000000000');
        const feesBalance = await mlpContractInstance.balanceOf(stakingFeeAddress);
        assert.equal(feesBalance, expectedFee, "Incorrect fee allocated");
    });

    it('should have accumulated correct rewards value up till 30d for others', async () => {
        await setBlockTime(1777945389, contractsOwner);

        const updatedTimestamp = (await web3.eth.getBlock('latest')).timestamp;
        assert.ok(+updatedTimestamp >= 1777945389 && +updatedTimestamp <= 1777945391, "Invalid timestamp");

        const pendingRewards = await lpContractInstance.earned(othersAddress, {from: othersAddress});
        console.log(pendingRewards.toString());
        assert.ok(
            pendingRewards.gte(new BN("11186000000000000000000")) && pendingRewards.lte(new BN("11804000000000000000000"))
        , "Invalid reward");
    })

    it('should unstake correctly without fee ', async () => {
        // Set the contract to ignore fees.
        const emissionRate = BigInt(5787036000000000);
        const isFeesApply = false;
        
        await lpContractInstance.setRewardRate(emissionRate, isFeesApply, {from: contractsOwner});
        
        const rewardRateSet = await lpContractInstance.rewardRate();
        const isFeesApplySet = await lpContractInstance.applyFees();
        
        assert.equal(rewardRateSet.toString(), emissionRate.toString(), "The reward rate is not set correctly");
        assert.equal(isFeesApplySet, isFeesApply, "The apply fees flag is not set correctly");

        const initialBalance = await lpContractInstance.balanceOf(userAddress);

        const unstakeAmount = new BN(BigInt(1 * 1e18));      
        await lpContractInstance.unstake(unstakeAmount.toString(), {from: userAddress});
        
        const finalBalance = await lpContractInstance.balanceOf(userAddress);
        assert.equal(finalBalance.toString(), initialBalance.sub(unstakeAmount).toString(), "Balance in LP wasn't deducted properly");

        // Validate fee taken (acccumualtive of previous tests). 
        // User balance is validated at after all deductions.
        const expectedFee = BigInt('895000000000000000');
        const feesBalance = await mlpContractInstance.balanceOf(stakingFeeAddress);
        assert.equal(feesBalance, expectedFee, "Incorrect fee allocated");

        const userFinalBalance = await mlpContractInstance.balanceOf(userAddress);
        assert.equal(userFinalBalance.toString(), '407105000000000000000', 'Unstake added up incorrectly');
    });

    it('should not affect accumulated claim rewards when reward rate is updated by owner', async () => {
        const oldRewardAmount = await lpContractInstance.earned(userAddress, {from: userAddress});

        const emissionRate = BigInt(0);
        const isFeesApply = true;
        
        await lpContractInstance.setRewardRate(emissionRate, isFeesApply, {from: contractsOwner});
        
        const rewardRateSet = await lpContractInstance.rewardRate();
        const isFeesApplySet = await lpContractInstance.applyFees();
        
        assert.equal(rewardRateSet.toString(), emissionRate.toString(), "The reward rate is not set correctly");
        assert.equal(isFeesApplySet, isFeesApply, "The apply fees flag is not set correctly");

        const newRewardAmount = await lpContractInstance.earned(userAddress, {from: userAddress});
        assert.equal(oldRewardAmount.toString(), newRewardAmount.toString(), "Reward rate affected claim amounts");
    });

    it('should claim rewards correctly with pending rewards', async () => {
        const rewardAmount = await lpContractInstance.earned(userAddress, {from: userAddress});

        const initialBalance = await elmContractInstance.balanceOf(userAddress);
        const expectedFinalBalance = initialBalance.add(rewardAmount)

        // Claims accrued rewards.
        await lpContractInstance.getReward({from: userAddress});
        
        const finalReward = await lpContractInstance.earned(userAddress, {from: userAddress});
        assert.equal(finalReward.toString(), '0', "The claim rewards function does not work correctly");

        const finalUserBalance = await elmContractInstance.balanceOf(userAddress);
        assert.equal(finalUserBalance.toString(), expectedFinalBalance.toString(), "User balance wasn't added correctly");
    });

    it('should claim rewards correctly with no rewards', async () => {
        const rewardAmount = await lpContractInstance.earned(emptyAddress, {from: emptyAddress});
        assert.equal(rewardAmount.toString(), '0', "There is still pending rewards");

        const initialBalance = await elmContractInstance.balanceOf(emptyAddress);

        // Claims accrued rewards.
        await lpContractInstance.getReward({from: emptyAddress});
        
        const finalReward = await lpContractInstance.earned(emptyAddress, {from: emptyAddress});
        assert.equal(finalReward.toString(), '0', "The claim rewards function does not work correctly");

        const finalUserBalance = await elmContractInstance.balanceOf(emptyAddress);
        assert.equal(finalUserBalance.toString(), initialBalance.toString(), "User balance wasn't added correctly");
    });

    it('should unstake all correctly', async () => {
        const account = accounts[1];
        const initialBalance = await lpContractInstance.balanceOf(account);
        await lpContractInstance.unstakeAll({from: account});
        
        const finalBalance = await lpContractInstance.balanceOf(account);
        assert.equal(finalBalance.toString(), '0', "The unstake all function does not work correctly");
    });

    it('should not unstake more than the balance', async () => {
        const stakeAmount = new BN(BigInt(100 * 1e18));

        try {
            await lpContractInstance.unstake(stakeAmount.toString(), {from: userAddress});
            assert.fail("The transaction did not fail as expected");
        } catch (error) {
            assert.ok(error.toString().includes('revert'), "The error message should contain 'revert'");
        }
    });
});