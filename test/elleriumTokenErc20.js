const ElmToken = artifacts.require("ElleriumTokenERC20v2");
const BN = require('bn.js');

contract('ElleriumTokenERC20v2', (accounts) => {
    const maxUint256 = (new BN(2)).pow(new BN(256)).sub(new BN(1));

    const contractsOwner = accounts[0];
    const userAddress = accounts[1];
    let elmContractInstance;

    before(async () => {
        // Deploy elmContractInstance.
        elmContractInstance = await ElmToken.new({ from: contractsOwner });
    });

    it("should check total supply at deployment", async function() {
        const totalSupply = await elmContractInstance.totalSupply();
        const expectedSupply = new BN('1175550000000000000000000');
        assert.equal(totalSupply.toString(10), expectedSupply.toString(10), "total supply is not as expected");
    });

    it("should transfer the right amount", async function() {
        const amountToTransfer = new BN(10);
        const expectedBalanceOfAccount1 = (await elmContractInstance.balanceOf(userAddress)).add(amountToTransfer);
        await elmContractInstance.transfer(userAddress, amountToTransfer, { from: contractsOwner });
        const balanceOfAccount1 = await elmContractInstance.balanceOf(userAddress);
        assert.equal(balanceOfAccount1.toString(), expectedBalanceOfAccount1.toString());
    });

    it("should fail when transfer value exceeds balance", async function() {
        const amountToTransfer = new BN(1000);
        try {
            await elmContractInstance.transfer(accounts[2], amountToTransfer, { from: userAddress });
            assert.fail('Expected revert not received');
        } catch (error) {
            assert(error.message, 'ERC20: transfer amount exceeds balance');
        }
    });

    it("should fail when non-owner tries to mint", async function() {
        const amountToMint = new BN(10);
        try {
            await elmContractInstance.mint(userAddress, amountToMint, { from: userAddress });
            assert.fail('Expected revert not received');
        } catch (error) {
            assert(error.message, 'ERC20: mint from non-approved address');
        }
    });

    it("should allow owner to mint", async function() {
        const oldBalanceOfAccount1 = await elmContractInstance.balanceOf(userAddress);
        const amountToMint = new BN(10);
        await elmContractInstance.mint(userAddress, amountToMint, { from: contractsOwner });
        const balanceOfAccount1 = await elmContractInstance.balanceOf(userAddress);
        assert.equal(balanceOfAccount1.toString(), oldBalanceOfAccount1.add(amountToMint).toString());
    });

    it("should allow user to approve spender", async function() {
        const approveAmount = new BN(10);
        await elmContractInstance.approve(userAddress, approveAmount, { from: contractsOwner });
        const allowanceAmount = await elmContractInstance.allowance(contractsOwner, userAddress);
        assert.equal(allowanceAmount.toString(), approveAmount.toString());
    });

    it("should fail when spender tries to spend more than approved amount", async function() {
        const approveAmount = new BN(10);
        const spendAmount = new BN(20);
        await elmContractInstance.approve(userAddress, approveAmount, { from: contractsOwner });
        try {
            await elmContractInstance.transferFrom(contractsOwner, accounts[3], spendAmount, { from: userAddress });
            assert.fail('Expected revert not received');
        } catch (error) {
            assert(error.message, 'ERC20: transfer amount exceeds allowance');
        }
    });

    it("should allow spender to spend less or equal to approved amount", async function() {
        const approveAmount = new BN(30);
        const spendAmount = new BN(20);
        await elmContractInstance.approve(userAddress, approveAmount, { from: contractsOwner });
        await elmContractInstance.transferFrom(contractsOwner, accounts[3], spendAmount, { from: userAddress });
        const balanceOfAccount3 = await elmContractInstance.balanceOf(accounts[3]);
        assert.equal(balanceOfAccount3.toString(), spendAmount.toString());
    });

    it("should reduce allowance after spender spends some amount", async function() {
        const approveAmount = new BN(50);
        const spendAmount = new BN(20);
        await elmContractInstance.approve(userAddress, approveAmount, { from: contractsOwner });
        await elmContractInstance.transferFrom(contractsOwner, accounts[3], spendAmount, { from: userAddress });
        const remainingAllowance = await elmContractInstance.allowance(contractsOwner, userAddress);
        assert.equal(remainingAllowance.toString(), approveAmount.sub(spendAmount).toString());
    });

    it("should fail when spender tries to spend after owner reduces allowance", async function() {
        const approveAmount = new BN(50);
        const reducedApproveAmount = new BN(20);
        const spendAmount = new BN(30);
        await elmContractInstance.approve(userAddress, approveAmount, { from: contractsOwner });
        await elmContractInstance.approve(userAddress, reducedApproveAmount, { from: contractsOwner });
        try {
            await elmContractInstance.transferFrom(contractsOwner, accounts[3], spendAmount, { from: userAddress });
            assert.fail('Expected revert not received');
        } catch (error) {
            assert(error.message, 'ERC20: transfer amount exceeds allowance');
        }
    });

    it("should fail when user tries to transfer tokens leading to underflow", async function() {
        const contractsOwnerBalance = await elmContractInstance.balanceOf(contractsOwner);
        const transferAmount = contractsOwnerBalance.add(new BN(1)); // Will cause underflow
        try {
            await elmContractInstance.transfer(userAddress, transferAmount, { from: contractsOwner });
            assert.fail('Expected revert not received');
        } catch (error) {
            assert(error.message, 'ERC20: transfer amount exceeds balance');
        }
    });

    it("should fail when owner tries to mint tokens leading to overflow", async function() {
        try {
            await elmContractInstance.mint(contractsOwner, maxUint256, { from: contractsOwner }); // Assume contractsOwner has enough gas
            const mintAmount = new BN(1); // Will cause overflow
            await elmContractInstance.mint(contractsOwner, mintAmount, { from: contractsOwner });
            assert.fail('Expected revert not received');
        } catch (error) {
            assert(error.message, 'ERC20: mint amount overflows');
        }
    });

    it("should fail when user tries to approve tokens leading to overflow", async function() {
        await elmContractInstance.approve(userAddress, maxUint256, { from: contractsOwner });
        const approveAmount = new BN(1); // Will cause overflow
        try {
            await elmContractInstance.approve(userAddress, approveAmount, { from: contractsOwner });
            assert.fail('Expected revert not received');
        } catch (error) {
            assert(error.message, 'ERC20: approved amount overflows');
        }
    });

    it("should allow user to 'burn' tokens by sending to dead address", async function() {
        const deadAddress = '0x0000000000000000000000000000000000000000';
        const burnAmount = new BN(10);
        const totalSupplyBeforeBurn = await elmContractInstance.totalSupply();
        const expectedSupply = totalSupplyBeforeBurn.sub(burnAmount);
        await elmContractInstance.transfer(deadAddress, burnAmount, { from: contractsOwner });
        const totalSupplyAfterBurn = await elmContractInstance.totalSupply();
        assert.equal(totalSupplyAfterBurn.toString(), expectedSupply.toString());
    });
    
    it("should fail when user tries to 'burn' more tokens than they have by sending to dead address", async function() {
        const deadAddress = '0x0000000000000000000000000000000000000000';
        const contractsOwnerBalance = await elmContractInstance.balanceOf(contractsOwner);
        const burnAmount = contractsOwnerBalance.add(new BN(1));
        try {
            await elmContractInstance.transfer(deadAddress, burnAmount, { from: contractsOwner });
            assert.fail('Expected revert not received');
        } catch (error) {
            assert(error.message, 'ERC20: transfer amount exceeds balance');
        }
    });
});
